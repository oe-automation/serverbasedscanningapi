﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace OEScanMiddleware.Controllers
{
    public class RegisterProviderController : ApiController
    {
        // POST: api/RegisterProvider
        public string Post([FromBody]string Url)
        {
            // Get client ID and value
            var ApiBroker = (APIBroker)HttpContext.Current.Application["APIBroker"];

            // Add the provider in here.
            bool RegisterPassed = ApiBroker.Register(Url);
            return RegisterPassed ? "PROVIDER WAS REGISTERED OK!" : "FAILED TO REGISTER PROVIDER TO MIDDLEWARE!";
        }
    }
}
