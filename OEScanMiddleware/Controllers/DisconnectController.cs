﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace OEScanMiddleware.Controllers
{
    public class DisconnectController : ApiController
    {
        // GET: api/Disconnect/5
        public string Get(int ClientId)
        {
            // Get client ID and value
            string ClientIdString = ClientId.ToString();
            var ApiBroker = (APIBroker)HttpContext.Current.Application["APIBroker"];

            // Unregister here.
            bool DcPassed = ApiBroker.DisconnectClient(ClientIdString);
            return DcPassed ? "DISCONNECTED THE DESIRED CLIENT OK!" : "FAILED TO DISCONNECT THE GIVEN CLIENT";
        }
    }
}
