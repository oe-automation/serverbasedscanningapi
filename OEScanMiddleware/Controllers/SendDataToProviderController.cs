﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using OEScanMiddleware.Models;

namespace OEScanMiddleware.Controllers
{
    public class SendDataToProviderController : ApiController
    {
        // POST: api/SendDataToProvider
        public string Post([FromBody]dynamic JsonInput)
        {
            // Timer
            var Timer = new Stopwatch();
            Timer.Start();

            // Get client ID and value
            string ClientId = JsonInput.ClientId;
            string TransactionId = JsonInput.TransactionId;
            string Message = JsonInput.Message;
            var ApiBroker = (APIBroker)HttpContext.Current.Application["APIBroker"];

            // Subscribe and make message request.
            var Result = ApiBroker.SendDataToProvider(ClientId, TransactionId, Message);
            Timer.Stop();

            // Insert new Timing Log values
            using (var db = new ServerBasedScanningEntities())
            {
                var dbgLog = db.Set<TimingLog>();
                dbgLog.Add(new TimingLog()
                {
                    ClientId = int.Parse(ClientId), 
                    TransactionId = int.Parse(TransactionId),
                    UnixMillEpoch = Timer.ElapsedMilliseconds,
                    IsMiddleWare = true,
                    Message = "TIME OF POST"
                });
                db.SaveChanges();
            }
            using (var db = new ServerBasedScanningEntities())
            {
                var dbgLog = db.Set<TimingLog>();
                dbgLog.Add(new TimingLog()
                {
                    ClientId = int.Parse(ClientId),
                    TransactionId = int.Parse(TransactionId),
                    UnixMillEpoch = long.Parse(Result),
                    IsMiddleWare = true,
                    Message = "TIME OF BROKER"
                });
                db.SaveChanges();
            }

            // Return time values.
            return $"{Timer.ElapsedMilliseconds} | {Result}";
        }
    }
}
