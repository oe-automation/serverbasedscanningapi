﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OEScanMiddleware.ServiceHelpers
{
    public interface IAPIDelegate
    {
        string MakeGETCall(string Url, ApiHeader HeaderValue);
        string MakePOSTCall(string Url, object MessageObject, ApiHeader HeaderValue);
        string MakePUTCall(string Url, object MessageObject, ApiHeader HeaderValue);
    }
}