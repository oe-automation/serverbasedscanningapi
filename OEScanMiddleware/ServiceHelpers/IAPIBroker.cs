﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OEScanMiddleware.ServiceHelpers
{
    public interface IAPIBroker
    {
        bool Register(string ClientId);
        bool DisconnectClient(string ClientId);
        string SendDataToProvider(string ClientId, string TransactionId, string ApiBaseURL);
    }
}