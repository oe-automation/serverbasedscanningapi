﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Microsoft.Ajax.Utilities;
using OEScanMiddleware.ServiceHelpers;

namespace OEScanMiddleware
{
    public class APIBroker : IAPIBroker
    {
        public APIDelegate Delegate;
        public Dictionary<string, bool> Providers;        // Mapped scan agents
        public Dictionary<string, string> Connections;    // Clients mapped.

        public APIBroker()
        {
            this.Delegate = new APIDelegate(); 
            Providers = new Dictionary<string, bool>();
            Connections = new Dictionary<string, string>();
        }

        // ------------------------------------------------------------------------------

        public bool Register(string ArgString)
        {
            // Init a new API Delegate and add instance to mapping.
            Providers.Add(ArgString, false);

            // Return here.
            return true;
        }
        public bool DisconnectClient(string ClientId)
        {
            // Find the client and pull it off.
            // var AllocatedAgents = RegisteredClients.Where(PairObj => PairObj.Key == ClientId).ToList();
            // var AllocatedClients = RegisteredScanAgents.Where(PairObj => PairObj.Value == true);

            // CANT SET DICT VALUES!!
            // AllocatedClients.ForEach(PairObj => PairObj.Value = false);
            // AllocatedClients[ClientId] = false;
            return true;
        }


        public string SendDataToProvider(string ClientId, string TransactionId, string MessageValue)
        {
            // TODO: Optimize Null object for zero providers
            if (!Connections.Any(PairObj => PairObj.Key == ClientId))
            {
                // Get next provider and register it.
                var NextProvider = Providers.FirstOrDefault(Prov => Prov.Value == false);
                Connections.Add(ClientId, NextProvider.Key);
            }

            // Get APi URL.
            var ApiURL = Connections.FirstOrDefault(PairObj => PairObj.Key == ClientId);
            string RequestURL = ApiURL.Value + "/api/ProcessClientMessage";

            // Make the request here.
            var ApiCallResponse = Delegate.MakePOSTCall(
                RequestURL, 
                String.Format("{0};{1};{2}", ClientId, TransactionId, MessageValue),
                new ApiHeader()
            );

            // Return output from command.
            return ApiCallResponse;
        }
    }
}
