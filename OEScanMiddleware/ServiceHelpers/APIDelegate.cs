﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestSharp;

namespace OEScanMiddleware.ServiceHelpers
{
    public class APIDelegate : IAPIDelegate
    {
        public string MakeGETCall(string Url, ApiHeader HeaderValue)
        {
            // Download the response from the RestSharp Client .
            var client = new RestClient(Url);
            var request = new RestRequest(Method.GET);

            // Get the Header URL Value.
            request.AddHeader(HeaderValue.name, HeaderValue.value);
            IRestResponse response = client.Execute(request);

            // Return a new API Response Object.
            return response.Content;
        }
        public string MakePOSTCall(string Url, object MessageObject, ApiHeader HeaderValue)
        {
            // Download the response from the RestSharp Client .
            var client = new RestClient(Url);
            var request = new RestRequest(Method.POST);

            // Get the Header URL Value.
            request.AddHeader(HeaderValue.name, HeaderValue.value);
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(MessageObject);
            IRestResponse response = client.Execute(request);

            // Return a new API Response Object.
            return response.Content;
        }
        public string MakePUTCall(string Url, object MessageObject, ApiHeader HeaderValue)
        {
            // Temp Return.
            return "";
        }
    }
}