﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OEScanMiddleware.ServiceHelpers
{
    public class ApiHeader
    {
        public string name = "Content-Type";
        public string value = "application/json";
    }
}