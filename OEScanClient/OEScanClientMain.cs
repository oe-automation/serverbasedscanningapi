﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OEScanClient.Models;
using RestSharp;

namespace OEScanClient
{
    public class OEScanClientMain
    {
        // Main Url for the Middleware API
        public static string BaseURL = ConfigurationManager.AppSettings["ApiBaseURL"];

        /// <summary>
        /// Runs a set of calls to the Middleware.
        /// Continues forever.
        /// </summary>
        /// <param name="args">Args for the program. Not used.</param>
        public static void Main(string[] args)
        {
            // Keep looping.
            while (true)
            {           
                // Timer
                var Timer = new Stopwatch();
                Timer.Start();

                // Generate random client and tx values.
                var RngObj = new Random();
                int RandomClient = RngObj.Next(1, 999);
                int RandomTx = RngObj.Next(1000, 100000);

                // Build the content for the body of the call
                JObject JsonBody = JObject.FromObject(new
                {
                    ClientId = RandomClient,
                    TransactionId = RandomTx,
                    Message = $"MESSAGE FOR {RandomClient}, TRANSACTION ID OF {RandomTx}",
                });

                // Insert new Timing Log values
                using (var db = new ServerBasedScanningEntities())
                {
                    var dbgLog = db.Set<DebugLog>();
                    dbgLog.Add(new DebugLog()
                    {
                        ClientId = RandomClient,
                        TransactionId = RandomTx,
                        TimeStamp = DateTime.Now,
                        IsMiddleWare = false,
                        Message = $"MESSAGE FOR {RandomClient}, TRANSACTION ID OF {RandomTx}"
                    });
                    db.SaveChanges();
                }

                // Send the Post call here.
                MakePOSTCall(BaseURL, JsonBody);

                // Log timer info out to the SQL Table.
                Timer.Stop();
                using (var db = new ServerBasedScanningEntities())
                {
                    var dbgLog = db.Set<TimingLog>();
                    dbgLog.Add(new TimingLog()
                    {
                        ClientId = RandomClient,
                        TransactionId = RandomTx,
                        UnixMillEpoch = Timer.ElapsedMilliseconds,
                        IsMiddleWare = false,
                        Message = "TIME OF CLIENT POST"
                    });
                    db.SaveChanges();
                }

                // Check for continue
                Console.WriteLine("CONTINUE?");
                string ReadIn = Console.ReadLine();
                if (ReadIn.Length != 0 && (!ReadIn.Contains("1") && !ReadIn.ToLower().Contains("t"))) { break;  }
            }
        }

        /// <summary>
        /// Send Data to MW API
        /// </summary>
        /// <param name="Url">Base URL</param>
        /// <param name="MessageObject">Message to send</param>
        /// <returns>String of API Content.</returns>
        public static string MakePOSTCall(string Url, object MessageObject)
        {
            // Download the response from the RestSharp Client .
            var ApiClient = new RestClient(Url);
            var ApiRequest = new RestRequest("api/SendDataToProvider", Method.POST);
            ApiClient.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;

            // Get the Header URL Value.
            ApiRequest.AddHeader("Content-Type", "application/json");
            ApiRequest.AddJsonBody(JsonConvert.SerializeObject(MessageObject));
            IRestResponse ApiResponse = ApiClient.Execute(ApiRequest);

            // Return a new API Response Object.
            return ApiResponse.Content;
        }
    }
}
