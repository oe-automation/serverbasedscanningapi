﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using RestSharp;

namespace OEScanProvider
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
 
            // Register host to MW
            var ProviderURL = WebConfigurationManager.AppSettings["ProviderURL"];
            var MiddleURL = WebConfigurationManager.AppSettings["MiddleWareURL"];
            RegisterHost(MiddleURL, ProviderURL);
        }

        /// <summary>
        /// Registers this provider to the middleware API
        /// </summary>
        /// <param name="MiddleWareURL">URL of the middleware</param>
        /// <param name="ProviderURL">IP Of the provider (Self)</param>
        public static void RegisterHost(string MiddleWareURL, string ProviderURL)
        {
            // Download the response from the RestSharp Client .
            var ClientObject = new RestClient(MiddleWareURL);
            var RequestObject = new RestRequest("api/RegisterProvider", Method.POST);

            // Get the Header URL Value.
            RequestObject.AddJsonBody(ProviderURL);
            IRestResponse response = ClientObject.Execute(RequestObject);
        }
    }
}
