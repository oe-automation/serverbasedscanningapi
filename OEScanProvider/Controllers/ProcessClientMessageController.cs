﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using OEScanProvider.Models;

namespace OEScanProvider.Controllers
{
    public class ProcessClientMessageController : ApiController
    {
        // POST: api/ProcessClientMessage
        public long Post([FromBody] string inputVal)
        {
            // Split the string args out.
            var args = inputVal.Split(';');

            // Timer
            var Timer = new Stopwatch();
            Timer.Start();

            // Insert new Timing Log values
            using (var db = new ServerBasedScanningEntities())
            {
                var dbgLog = db.Set<DebugLog>();
                dbgLog.Add(new DebugLog()
                {
                    ClientId = int.Parse(args[0]),
                    TransactionId = int.Parse(args[1]),
                    TimeStamp = DateTime.Now,
                    IsMiddleWare = false,
                    Message = args[2]
                });
                db.SaveChanges();
            }

            // Stop watch
            Timer.Stop();

            // Log timer info out to the SQL Table.
            using (var db = new ServerBasedScanningEntities())
            {
                var dbgLog = db.Set<TimingLog>();
                dbgLog.Add(new TimingLog()
                {
                    ClientId = int.Parse(args[0]),
                    TransactionId = int.Parse(args[1]),
                    UnixMillEpoch = Timer.ElapsedMilliseconds,
                    IsMiddleWare = false,
                    Message = "TIME OF PROVIDER POST"
                });
                db.SaveChanges();
            }

            // Return time taken for post. 
            return Timer.Elapsed.Milliseconds;
        }
    }
}
